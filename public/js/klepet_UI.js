/**
 * 
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://teaching.lavbic.net/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://teaching.lavbic.net/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://teaching.lavbic.net/OIS/gradivo/") > -1;
  var omemba=false;
  if(sporocilo.indexOf(" (zasebno): &#9758; Omemba v klepetu")!==-1){
    omemba=true;
  }
  if (jeSmesko) {
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else if(omemba){
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div class="div" style="color:'+barva+'; font-weight:bold"></div>').text(sporocilo);  
  }
}

function divElementSeznami(input){
  var jeSmesko = input.indexOf("https://teaching.lavbic.net/OIS/gradivo/") > -1;
   if (jeSmesko) {
    input =
      input.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlSeznam(input);
  } else{
    return $('<div style="font-weight:bold"></div>').text(input);
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div class="div" style="color:'+barva+'"></div>').html('<i>' + sporocilo + '</i>');
}

function divElementHtmlSeznam(input){
  return $('<div></div>').html('<i>' + input + '</i>');
}

/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();

  var tag=sporocilo.split(' ');
  var linki=sporocilo.split(' ');
  linki=linki.filter(element => element.indexOf('https://www.youtube.com/watch?v=')!==-1);
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  if (sporocilo.charAt(0) == '/') {
    tag=tag.filter(word => (word.charAt(0)==='@'||word.charAt(1)==='@'));
    for(var i in tag){
      tag[i]=tag[i].substring(1);
      if(tag[i].indexOf('\"')!==-1){
        tag[i]=tag[i].substring(0,tag[i].length-1);
      }
      if(tag[i].indexOf('@')!==-1){
        tag[i]=tag[i].substring(1,tag[i].length);
      }
    }
    var users=$('#seznam-uporabnikov').children();
    for(var i=0; i<users.length; i++){
      users[i]=users[i].innerHTML;
    }
    for(var k=0; k<tag.length; k++){
      if(tag[k]===trenutniVzdevek){
        tag.splice(k,1);
        continue;
      }
      var exists=false;
      for(var j in users){
        if(users[j]===tag[i]){
          exists=true;
          break;
        }
      }
      if(!exists){
        tag.splice(i,1);
      }
    }
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      for(var i=0; i<tag.length; i++){
        var generateprivate='/zasebno "'+tag[i]+'" "&#9758; Omemba v klepetu"';
        klepetApp.procesirajUkaz(generateprivate);
      }
      for(var i=0; i<linki.length; i++){
        linki[i]=linki[i].split("=");
        linki[i]=linki[i][1];
        linki[i]=linki[i].substring(0,11);
        $('#sporocila').append("<iframe style='width:200px height:150px margin-top:10px margin-left:10px' src='https://www.youtube.com/embed/"+linki[i]+"' allowfullscreen></iframe>");
      }
    }
  } else{
      tag=tag.filter(word => word.charAt(0)==='@');
      for(var i in tag){
        tag[i]=tag[i].substring(1);
      }
      var users=$('#seznam-uporabnikov').children();
      for(var i=0; i<users.length; i++){
        users[i]=users[i].innerHTML;
      }
      for(var k=0; k<tag.length; k++){
        if(tag[k]===trenutniVzdevek){
          tag.splice(k,1);
          continue;
        }
        var exists=false;
        for(var j in users){
          if(users[j]===tag[i]){
            exists=true;
            break;
          }
        }
        if(!exists){
          tag.splice(i,1);
        }
      }
      sporocilo = filtrirajVulgarneBesede(sporocilo);
      klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      for(var i=0; i<tag.length; i++){
        var generateprivate='/zasebno "'+tag[i]+'" "&#9758; Omemba v klepetu"';
        klepetApp.procesirajUkaz(generateprivate);
      }
      for(var i=0; i<linki.length; i++){
        linki[i]=linki[i].substring(32);
        $('#sporocila').append("<iframe style='width:200px height:150px margin-top:10px margin-left:10px' src='https://www.youtube.com/embed/"+linki[i]+"' allowfullscreen></iframe>");
      }
    }
  $('#poslji-sporocilo').val('');
}


// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('./swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});


/**
 * Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj 
 * z enako dolžino zvezdic (*)
 * 
 * @param vhodni niz
 */
function filtrirajVulgarneBesede(vhod) {
  for (var i in vulgarneBesede) {
    var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
    vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
  }
  return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var barva="black";


// Počakaj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  socket.on('barvaSpremembaOdgovor',function(rezultat){
    barva=rezultat.barva;
    $('#sporocila').find(".div").css("color",barva);
    $('#kanal').css("color",barva);
  });
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var linki=sporocilo.besedilo.split(' ');
    linki=linki.filter(element => element.substring(0,32)==='https://www.youtube.com/watch?v=');
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    for(var i=0; i<linki.length; i++){
      linki[i]=linki[i].substring(32);
      $('#sporocila').append("<iframe style='width:200px height:150px margin-top:10px margin-left:10px' src='https://www.youtube.com/embed/"+linki[i]+"' allowfullscreen></iframe>");
    }
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementSeznami(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $("#seznam-uporabnikov").empty();
    for (var i=0; i < uporabniki.length; i++) {
      $("#seznam-uporabnikov").append(divElementSeznami(uporabniki[i]));
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

/* global $, io, Klepet */